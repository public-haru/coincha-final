output "role_arn" {
  value = aws_iam_role.assume_role.arn
}

output "user_arn" {
  value = aws_iam_user.user.arn
}

output "group_arn" {
  value = aws_iam_group.this.arn
}

output "policy_arn" {
  value = aws_iam_policy.this.arn
}

output "role_name" {
  value = aws_iam_role.assume_role.name
}

output "user_name" {
  value = aws_iam_user.user.name
}

output "group_name" {
  value = aws_iam_group.this.name
}

output "policy_name" {
  value = aws_iam_policy.this.name
}


