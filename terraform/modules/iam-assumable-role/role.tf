data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
      "sts:TagSession",
      "sts:SetSourceIdentity"
    ]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    effect    = "Allow"
    actions   = ["sts:AssumeRole"]
    resources = [aws_iam_role.assume_role.arn]
  }
}

resource "aws_iam_policy" "this" {
  name        = "${var.environment_name}-policy"
  description = "Allows to assume role in another AWS account"
  policy      = data.aws_iam_policy_document.assume_role_policy.json

}

resource "aws_iam_role" "assume_role" {
  name               = "${var.environment_name}-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  tags               = {}
}

resource "aws_iam_group" "this" {
  name = "${var.environment_name}-group"
}

resource "aws_iam_group_policy_attachment" "this" {
  group      = aws_iam_group.this.id
  policy_arn = aws_iam_policy.this.id
}

resource "aws_iam_user" "user" {
  name = "${var.environment_name}-user"

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_group_membership" "team" {
  name = "${var.environment_name}-membership"

  users = [aws_iam_user.user.name]

  group = aws_iam_group.this.name
}