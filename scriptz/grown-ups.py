#!/usr/bin/env python3
import socket
import argparse


def count_hits(logfile):
    # create hashmap to store data
    ip_list = {}
    file = open(logfile, "r")

    # loop for lines
    for line in file:
        # get the ip
        ip = line.split(" ")[1]

        try:
            socket.inet_aton(ip)
            ip_list[ip] = ip_list.get(ip, 0) + 1
        except socket.error:
            print("Skipping IP {}".format(ip))

    return ip_list


parser = argparse.ArgumentParser()
parser.add_argument("filename", help="filename to parse")
args = parser.parse_args()

result = count_hits(args.filename)
print(sorted(  ( (value,key) for (key,value) in result.items()) ,reverse=True))
