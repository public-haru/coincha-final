#!/usr/bin/env bash

#check if file exists and only 1 arg supplied
if [ "$#" -ne 1 ] || ! [ -f "$1" ]; then
  echo "Usage: $0 FILE" >&2
  exit 1
fi

filename=$1

cat $filename | cut -d' ' -f2 | sort -n | uniq -c | sort -nr
