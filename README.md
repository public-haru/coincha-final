# DevOps Assessment 

## 1. Docker-ayes

Build:
```
docker build . -t bitcoinx
```

Scan:
```
docker scan --accept-license --severity medium bitcoinx
```

Tested using the default scan 

Running..:
```
docker run --rm bitcoinx
```


## 2. Kubernetes FTW:

you need to push it to some repository , like DockerHub

```
docker tag bitcoinx registry-1.docker.io/USERNAME/bitcoin-core:test
docker push registry-1.docker.io/USERNAME/bitcoin-core:test
```

For deploying..
```
kubectl apply -f kubernetes/bitcoin-core.yaml -n NAMESPACE
```
You may need to adapt your username in the yaml file..
[kubernetes/bitcoin-core.yaml](kubernetes/bitcoin-core.yaml)

## 3. All the continuouses:

I used official docker gcp sdk image that comes with kubectl and gcloud cli included

The variables GCLOUD_SERVICE_KEY, $GCP_PROJECT_ID are gitlab secrets, but ideally they should be in Vault

Default gitlab security scan is used as in [.gitlab-ci.yml](.gitlab-ci.yml) file.

[pipeline](https://gitlab.com/public-haru/coincha-final/-/pipelines)

Some screenshots of GKE..


![Services](picz/services.png)

![Pods](picz/pods.png)

![Deployments](picz/deployment.png)

## 4. Script Kiddies

Found in [scriptz/kiddies.sh](scriptz/kiddies.sh)

It validates that one parameter is supplies and outputs the list of ips in descending order

## 5. Script Grown Ups


Found in [scriptz/grown-ups.py](scriptz/grown-ups.py)

It validates that one parameter is supplies and outputs the list of ips in descending order

Works with python3

## 6. Terraform lovers unite

I created a module in here [terraform/modules/iam-assumable-role](terraform/modules/iam-assumable-role)

Also a `test-tf` file for testing

The module only has one parameter `environment_name` that is used as a prefix for the 4 resources.

## 7. Nomad Users WAGMI

Haven't done this task as I have zero experience with Nomad

